// import './App.css'
import cssModule from './App.module.css'

function App() {
  return (
    <div className={cssModule.devCamp}>
      <div className={cssModule.decampWrapper}>
      <img className={cssModule.devcampAvatar} src='https://randomuser.me/api/portraits/women/48.jpg' />

      <div className={cssModule.devcampQuoteP}>
      <p>this is one of the best developers blogs on the planet! i read it daily to improve my skils.</p>
      </div>
      <p className={cssModule.devcampName}><span className={cssModule.devcampNameSpan}>tammy Stevens.</span> front End developer</p>
      </div>
    </div>
  );
}

export default App;
